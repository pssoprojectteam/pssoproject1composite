
//Tree node that contains two children

public class BinaryTreeNode extends TreeNode {

	private TreeNode left;
	private TreeNode right;

	public BinaryTreeNode(String label) {
		super(label);
		left = right = null;
	}

	public BinaryTreeNode(String label, TreeNode left, TreeNode right) {
		super(label);
		this.left = left;
		this.right = right;
	}

	@Override
	public void inFixPrint() {
		System.out.print("( ");
		left.inFixPrint();
		System.out.print(" " + label + " ");
		right.inFixPrint();
		System.out.print(" )");
	}

	@Override
	public void postFixPrint() {
		left.postFixPrint();
		right.postFixPrint();
		System.out.print(label + " ");
	}

	@Override
	public void preFixPrint() {
		System.out.print(label + " ");
		left.preFixPrint();
		right.preFixPrint();
	}

	@Override
	public double evaluate() {
		if(label.equals("+")){
			return left.evaluate() + right.evaluate();
		}
		else if(label.equals("*")){
			return left.evaluate() * right.evaluate();
		}
		else if(label.equals("-")){
			return left.evaluate() - right.evaluate();
		}
		else if(label.equals("/")){
			return left.evaluate() / right.evaluate();
		}
		else if(label.equals("%")){
			return left.evaluate() % right.evaluate();
		}
		else
			return 0;
	}

	@Override
	public String generateInFixString() {
		StringBuilder sb = new StringBuilder();
		sb.append("( ");
		sb.append(left.generateInFixString());
		sb.append(" " + label + " ");
		sb.append(right.generateInFixString());
		sb.append(" )");

		return sb.toString();
	}

	@Override
	public String generatePostFixString() {
		StringBuilder sb = new StringBuilder();
		sb.append(left.generatePostFixString());
		sb.append(right.generatePostFixString());
		sb.append(label + " ");

		return sb.toString();
	}

	@Override
	public String generatePreFixString() {
		StringBuilder sb = new StringBuilder();
		sb.append(label + " ");
		sb.append(left.generatePreFixString());
		sb.append(right.generatePreFixString());

		return sb.toString();
	}
}