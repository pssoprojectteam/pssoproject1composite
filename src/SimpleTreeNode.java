import sun.reflect.generics.reflectiveObjects.NotImplementedException;

//Define tree element with no children

public class SimpleTreeNode extends TreeNode {

	public SimpleTreeNode(String label) {
		super(label);
	}

	@Override
	public void inFixPrint() {
		System.out.print(label + " ");
	}

	@Override
	public String generateInFixString() {
		return label;
	}

	@Override
	public void postFixPrint() {
		System.out.print(label);
	}

	@Override
	public void preFixPrint() {
		System.out.print(label);
	}

	@Override
	public double evaluate() {
		return Double.parseDouble(label);
	}

	@Override
	public String generatePostFixString() {
		return label + " ";
	}

	@Override
	public String generatePreFixString() {
		return label + " ";
	}
}