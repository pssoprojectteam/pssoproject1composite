import sun.reflect.generics.reflectiveObjects.NotImplementedException;

//abstract superclass of tree element types

public abstract class TreeNode  {

  protected TreeNode(String label)  {
    this.label = label;
  }

  public abstract void inFixPrint();  
    
  public abstract void postFixPrint(); 
  
  public abstract void preFixPrint();
  
  public abstract double evaluate();

  public abstract String generateInFixString();
  
  public abstract String generatePostFixString();
  
  public abstract String generatePreFixString();
  
  protected String label;
}