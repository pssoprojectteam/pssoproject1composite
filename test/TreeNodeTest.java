import static org.junit.Assert.*;

import org.junit.Test;

public class TreeNodeTest {

	@Test
	public void inFixTest() {
		Parser parser = new Parser();
		TreeNode rootNode = parser.parse("1 + ( 2 - 3 * 4 ) / 5");
		TreeNode rootNode2 = parser.parse("( 1 + 2 ) * ( 3 + 4 )");
		assertEquals("( 1 + ( ( 2 - ( 3 * 4 ) ) / 5 ) )", rootNode.generateInFixString());
		assertEquals("( ( 1 + 2 ) * ( 3 + 4 ) )", rootNode2.generateInFixString());
	}

	@Test
	public void postFixTest() {
		Parser parser = new Parser();
		TreeNode rootNode2 = parser.parse("( 1 + 2 ) * ( 3 + 4 )");
		assertEquals("1 2 + 3 4 + * ", rootNode2.generatePostFixString());
	}

	@Test
	public void preFixTest() {
		Parser parser = new Parser();
		TreeNode rootNode2 = parser.parse("( 1 + 2 ) * ( 3 + 4 )");
		assertEquals("* + 1 2 + 3 4 ", rootNode2.generatePreFixString());
	}

	@Test
	public void evaluateTest() {
		Parser parser = new Parser();
		TreeNode rootNode = parser.parse("1 + ( 2 - 3 * 4 ) / 5");
		TreeNode rootNode2 = parser.parse("( 1 + 2 ) * ( 3 + 4 )");
		
		assertEquals(-1.0, rootNode.evaluate(), 0.001);
		assertEquals(21.0, rootNode2.evaluate(), 0.001);
	}
}
